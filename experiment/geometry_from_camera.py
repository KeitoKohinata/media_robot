# v4l2-ctl  --list-devices
# v4l2-ctl -d /dev/video0 --list-formats-ext

import pathlib 
import sys

PARENT = pathlib.Path(__file__).resolve().parent
sys.path.append(str(PARENT))

import cv2
import numpy as np
from numpy import linalg as LA
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
import argparse
from tf_lib import quaternion_from_euler
from polygon import Triangle, Rectangle, ManageRectangles
from homography import Homography


# green hsv 養生テープ in my room 抽出用
LOWER_BOUND = np.array([50, 30, 40])
UPPER_BOUND = np.array([90, 255, 255])

TOPIC_NAME = "/localization"
# 結果として出力する画像の縦横サイズ
RESULT_SIZE = 500


class GeometryFromCam(Node):
    def __init__(self, mode="ros2"):
        """
        mode : ros2 or other
        """
        super().__init__("geometry_from_cam")
        self.mode = mode
        # マーカーの間隔は0.2m
        self.scale = 1
        # 場合によって変更する必要があるが，pcに繋いでいる以上変更する必要はない
        self.cam_number = 2
        self.cam_setting()
        self.window_setting()
        # ロボットのマーカーは中心にあると仮定
        self.pos = Triangle(init_area=0, init_center=(0.5,0.5), init_angle=0)
        # self.pos = Triangle(init_center=(self.cam_width/2, self.cam_height/2))
        self.marker_setting()
        self.hm = Homography()
        self.result_img = np.zeros([RESULT_SIZE,RESULT_SIZE], np.uint8)
        # ros2 init
        self.publisher = self.create_publisher(Odometry, TOPIC_NAME, 10)
        ## なるべく速く実行するため
        timer_period = 0.001 
        self.timer = self.create_timer(timer_period, self.timer_callback)
        # キャリブレーションを実行
        self.calibration()



    def cam_setting(self):
        # カメラの設定
        self.cam_width = 640
        self.cam_height = 480
        self.cam = cv2.VideoCapture(self.cam_number)
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, self.cam_width)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, self.cam_height)
        self.cam.set(cv2.CAP_PROP_FPS, 30)
        # print(f"{self.cam.get(cv2.CAP_PROP_FRAME_WIDTH)=}")
        # print(f"{self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT)=}")
        # print(f"{self.cam.get(cv2.CAP_PROP_FPS)=}")

    def writer_setting(selfl):
        fmt = cv2.VideoWriter_fourcc("m","p","4","v")
        # writerの用意
        result_writer = cv2.VideoWriter("result.mp4", fmt, 30, (RESULT_SIZE, RESULT_SIZE))
        
        
    def window_setting(self):
        # window
        cv2.namedWindow("img")
        cv2.namedWindow("img_bin")
        cv2.moveWindow("img", 50, 13)
        cv2.moveWindow("img_bin", 700, 50)
        # cv2.setWindowProperty("img", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        # cv2.setWindowProperty("img_bin", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    def marker_setting(self):
        aspect = [self.cam_width, self.cam_height]
        bi = aspect.index(max(aspect))
        si = aspect.index(min(aspect))
        # 正方形の1辺の長さ
        edge = aspect[si]
        self.edge = edge
        # marker の margin
        self.margin_marker = 0.1
        # marker の source． 変えない．
        self.src_pts_markers = np.array([
            [0,0],
            [1, 0],
            [1, 1],
            [0,1],
            [0.5,0],
        ], np.float64)
        # 1辺がedgeの正方形
        init_ceters = self.src_pts_markers*edge
        # 中央に平行移動
        init_ceters[:, bi] += int((aspect[bi]-edge)/2)
        # 中央に近づける
        init_ceters -= (self.src_pts_markers-0.5)*2*self.margin_marker*edge
        # print(f"{init_ceters=}")

        self.markers = [Rectangle(center, src) for center, src in zip(init_ceters, self.src_pts_markers)]
        self.manage_rects = ManageRectangles(self.markers)

    def calibration(self):
        # img, img_binを表示する．imgには，合わせるべきマーカーの位置を表示する．
        # 画像を回しながらロボットのいるべき座標と，マーカーの番号を画面に表示する．(calib状態)
        # 人間が，「それらが合わさった」と判断したなら，Enterを押す．（check状態への以降）
        # 画像前処理と多角形取得をして，ロボットの位置，マーカーの番号を表示する．（check状態)
            # それでよかったら，処理を終了する
            # よくなかったら，R を押してcalib状態に戻る．
        ret, img = self.cam.read()
        while ret:
            check_flag = False
            # calib
            while ret:
                self.preprocess_img(img)
                # マーカーを描画
                for marker in self.markers:
                    lt = (marker.center - self.edge*self.margin_marker/2).astype(int)
                    rb = (marker.center + self.edge*self.margin_marker/2).astype(int)
                    cv2.rectangle(img, lt, rb, (255,255,255), 3, cv2.LINE_AA)
                cv2.imshow("img", img)
                key = cv2.waitKey(1) 
                ret, img = self.cam.read()
                if key == ord("q"):
                    quit()
                elif key == ord("n"):
                    break
            # check
            while True:
                print("check")
                key = self.timer_callback()
                if key == ord("q"):
                    quit()
                elif key == ord("n"):
                    check_flag = True
                    break
                elif key == ord("r"):
                    break
            if check_flag:
                break
        pass

    def timer_callback(self):
        # 画像読み込み
        ret, img = self.cam.read()
        self.img = img
        # 読み込み不能の場合
        if not ret: 
            return 
        # 画像前処理
        img_bin = self.preprocess_img(img)
        # 多角形の取得
        polygon:dict = self.find_polygon(img_bin)
        # マーカーの位置を更新
        self.updated_markers = self.manage_rects.update(polygon.get(4,[]))
        # updateされたマーカーの描画 debug用
        for i,marker in enumerate(self.updated_markers):
            if marker.src_pt[0] == 0 and marker.src_pt[1] == 0:
                center = marker.center.astype("int")
                cv2.putText(img, str(i), center, cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 2, cv2.LINE_AA)
        # ロボットの位置・角度をupdate
        self.pos_update(polygon.get(3,[]))
        center, angle = self.pos_trans()
        
        # ロボットの位置・角度をpublishする
        if  self.mode == "ros2": 
            odom = Odometry()
            pose = Pose()
            odom.header.stamp = self.get_clock().now().to_msg()
            pose.position.x = center[0]
            pose.position.y = center[1]
            q = quaternion_from_euler(0,0,angle)
            pose.orientation.x = q[0]
            pose.orientation.y = q[1]
            pose.orientation.z = q[2]
            pose.orientation.w = q[3]
            odom.pose.pose = pose
            self.publisher.publish(odom)
            
        print(f"{center=}")
        print(f"{np.rad2deg(angle)=}")
        cv2.imshow("img", img)

        c = (self.pos.center * RESULT_SIZE).astype("int") % RESULT_SIZE
        self.result_img[c[1], c[0]] = 255
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11,11))
        result_img = cv2.dilate(self.result_img, kernel, iterations=1)
        cv2.imshow("result_img", result_img)

        key = cv2.waitKey(1)
        return key


    def pos_update(self, found_triangles):
        # ホモグラフィー行列の計算
        src_pts = np.array([marker.src_pt for marker in self.updated_markers], dtype=np.float64)
        dst_pts = np.array([marker.center for marker in self.updated_markers], dtype=np.float64)
        try :
            self.hm.get_homography(dst_pts, src_pts)
        except ValueError as e: # dst_pts の点数が不十分
            print(e)
            return
        # ロボットの現在位置・角度を更新
        found_triangles = [self.hm.warp_perspective(np.reshape(ft, [-1,2])) for ft in found_triangles]
        found_triangles = sorted(found_triangles, key=lambda x:self.pos.calc_dist(x), reverse=True)
        self.pos.update(found_triangles[0])

    def pos_trans(self):
        # center = (self.pos.center - np.array([0,1]))*self.scale*(-1)
        center = self.pos.center.copy()
        center[1] = (center[1] - 1)*(-1)
        center *= self.scale 
        # center = (self.pos.center)*self.scale
        angle = -self.pos.angle.copy()
        return center, angle

    
            

    def close(self):
        #TODO : save と 最終的な軌跡の表示？それは
        self.cam.release()
        cv2.destroyAllWindows()
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11,11))
        result_img = cv2.dilate(self.result_img, kernel, iterations=1)
        cv2.imwrite("result_img.png",result_img)
        cv2.imshow("result_img", result_img)
        cv2.waitKey()
        cv2.destroyAllWindows()
        pass

        

    def preprocess_img(self, img):
        """
        ぼかし，2値化等
        """
        ks = 9
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (ks,ks))
        img = cv2.GaussianBlur(img, (ks,ks), 0)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img = cv2.inRange(img, LOWER_BOUND, UPPER_BOUND)
        img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
        img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        # debug
        cv2.imshow("img_bin", img)
        return img
    
    # 多角形を見つける
    def find_polygon(self, img_bin):
        """
        Returns
        -------
        `result` : dict
            多角形のdict．{3:輪郭のリスト, 4:輪郭のリスト, ...} みたいな感じ．
        """
        _find_polygon_arclength_ratio = 0.1
        contours, _ = cv2.findContours(img_bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        result = dict()
        for cnt in contours:
            polygon:np.ndarray = cv2.approxPolyDP(cnt, _find_polygon_arclength_ratio*cv2.arcLength(cnt, closed=True), closed=True)
            result.setdefault(polygon.shape[0], [])
            result[polygon.shape[0]].append(polygon)
        return result


def parseOpt():
    """
    オプション処理
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("--mode", "-m", type=str, default="ros2")
    # 辞書化
    opt: dict = vars(parser.parse_args())
    return opt


def main(args=None):
    rclpy.init(args=args)
    opt = parseOpt()
    gfc = GeometryFromCam(opt["mode"])
    try :
        # ros2を使わないデバッグ時は --mode a とする．
        rclpy.spin(gfc)
    except Exception as e:
        print(e)
    finally :
        # after ctrl+c 
        gfc.close()
        gfc.destroy_node()
        rclpy.shutdown()




if __name__=="__main__":
    main()
# %%
