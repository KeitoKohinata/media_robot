import numpy as np
import numpy.linalg as LA

class Homography:
    def __init__(self):
        pass

    def get_homography(self, src_pts:np.ndarray, dst_pts:np.ndarray):
        """
        Parameters
        ----------
        `src_pts` : np.ndarray[ndim=2, dtype=np.float64]
            homography変換前の座標点
        `dst_pts` : np.ndarray[ndim=2, dtype=np.float64]    
            homography変換後の座標点
        """
        if src_pts.shape != dst_pts.shape:
            raise ValueError("src_pts.shape and dst_pts.shape are different.")
        if src_pts.shape[0] < 4:
            raise ValueError(f"pts must have at least 4 points\n{src_pts=}")

        A = np.zeros((src_pts.shape[0]*2,9), dtype=np.float64)
        for i, (a, b) in enumerate(zip(src_pts,dst_pts)):
            A[i * 2] = np.array([a[0], a[1], 1, 0, 0, 0, -b[0] * a[0], -b[0] * a[1], -b[0]])
            A[i * 2 + 1] = np.array([0, 0, 0, a[0], a[1], 1, -b[1] * a[0], -b[1] * a[1], -b[1]])

        A_ = A[:, :-1]
        b = -A[:,-1]
        # h_i = inv(A_)@b
        h_i = LA.inv(A_.T@A_)@A_.T@b
        h_i = np.concatenate([h_i, [1]], axis=0)
        self.hm = np.reshape(h_i, (3,3))

        test = self.hm@np.array([1,0,1])
        # test /= test[2]
        # print(test)

        return self.hm

    
    def warp_perspective(self, src_pts:np.ndarray):
        """
        Parameters
        ----------
        `src_pts` : np.ndarray[ndim=2, dtype=np.float64]
            変換したい座標

        Returns
        -------
        `dst_pts` : np.ndarray[ndim=2, dtype=np.float64]
            変換後の座標
        """
        if len(src_pts.shape) != 2:
            raise ValueError(f"{src_pts.shape=}")
        elif src_pts.shape[1] != 2:
            raise ValueError(f"{src_pts.shape[1]=}")
        
        # print("src_pts")
        # print(src_pts)
        ones = np.ones((src_pts.shape[0], 1), dtype=src_pts.dtype)
        src_pts = np.concatenate([src_pts, ones], axis=1)
        dst_pts = (self.hm @ src_pts.T).T
        for i in range(dst_pts.shape[0]):
            dst_pts[i] /= dst_pts[i][2]
        # print("dst_pts")
        # print(dst_pts)
        
        return dst_pts[:,:2]