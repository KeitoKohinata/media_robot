# %%
import os
import sys
import cv2
import yaml
import pathlib
import numpy as np

ROOT = str(pathlib.Path(__file__).absolute().parents[1])
win_size = 500

class OneStrokeWriting:
    def __init__(self):
        self.path = []
        cv2.namedWindow("img")
        cv2.setMouseCallback("img", self.drawCallback)
        cv2.moveWindow("img", 50, 50)
        self.img = np.zeros((win_size, win_size), np.uint8)
        cv2.imshow("img", self.img)
        key = -1
        # 保存しないでやめる場合
        while key != ord("q"): 
            key = cv2.waitKey(1)
        cv2.destroyAllWindows()
        
        
    def drawCallback(self, event, x,y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.path.append([x,y])
            if len(self.path) >= 2:
                cv2.line(self.img, self.path[-1], self.path[-2], 255, 3, cv2.LINE_AA)
            cv2.imshow("img", self.img)
        # 保存してやめる場合
        elif event == cv2.EVENT_RBUTTONDOWN: 
            cv2.destroyAllWindows()
            s = input("Input the name of saved image. -> ")
            self.save(s)
            sys.exit(0)

    def save(self, name):
        # 保存先の生成
        image_fold = "one_stroke_writing"
        os.makedirs(f"{ROOT}/{image_fold}", exist_ok=True)
        # 画像の保存
        img = cv2.cvtColor(self.img, cv2.COLOR_GRAY2BGR)
        cv2.imwrite(f"{ROOT}/{image_fold}/{name}.png", img)
        # 座標(0~1)の保存
        path = np.array(self.path)/win_size
        with open(f"{ROOT}/{image_fold}/{name}.yaml", "w") as f:
            data = {"path" : path.tolist()}
            yaml.dump(data, f)
        

        
        
if __name__=="__main__":
    OneStrokeWriting()
