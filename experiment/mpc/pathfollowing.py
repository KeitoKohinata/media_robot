import rclpy
from rclpy.node import Node
import tf2_py
from geometry_msgs.msg import PoseStamped, Twist, TransformStamped, Pose
from std_msgs.msg import Header
from std_srvs.srv import SetBool
from nav_msgs.msg import Path, Odometry
from mpc_pathfollower.MPC.mpc import *
from ABU2023_steer_sim.tf_lib.tf_lib import euler_from_quaternion, quaternion_from_euler
from tf2_ros.transform_broadcaster import TransformBroadcaster
from visualization_msgs.msg import Marker
import numpy as np
import math

# 目標状態(経路)
opath =[[0.509,0.063,0.   ],
 [0.531,0.063,0.   ],
 [0.554,0.063,0.   ],
 [0.574,0.065,0.   ],
 [0.596,0.065,0.   ],
 [0.619,0.065,0.   ],
 [0.641,0.065,0.   ],
 [0.663,0.065,0.   ],
 [0.685,0.065,0.   ],
 [0.707,0.065,0.   ],
 [0.73 ,0.065,0.   ],
 [0.752,0.065,0.   ],
 [0.77 ,0.067,0.   ],
 [0.793,0.067,0.   ],
 [0.815,0.067,0.   ],
 [0.837,0.067,0.   ],
 [0.856,0.069,0.   ],
 [0.856,0.08 ,0.   ],
 [0.856,0.091,0.   ],
 [0.856,0.102,0.   ],
 [0.857,0.113,0.   ],
 [0.857,0.124,0.   ],
 [0.857,0.135,0.   ],
 [0.857,0.146,0.   ],
 [0.857,0.157,0.   ],
 [0.857,0.169,0.   ],
 [0.857,0.18 ,0.   ],
 [0.857,0.191,0.   ],
 [0.857,0.202,0.   ],
 [0.857,0.213,0.   ],
 [0.857,0.224,0.   ],
 [0.857,0.235,0.   ],
 [0.857,0.246,0.   ],
 [0.857,0.257,0.   ],
 [0.857,0.269,0.   ],
 [0.857,0.28 ,0.   ],
 [0.857,0.291,0.   ],
 [0.857,0.302,0.   ],
 [0.857,0.313,0.   ],
 [0.859,0.324,0.   ],
 [0.859,0.335,0.   ],
 [0.859,0.346,0.   ],
 [0.859,0.357,0.   ],
 [0.859,0.369,0.   ],
 [0.859,0.38 ,0.   ],
 [0.859,0.391,0.   ],
 [0.859,0.402,0.   ],
 [0.859,0.413,0.   ],
 [0.859,0.424,0.   ],
 [0.859,0.435,0.   ],
 [0.859,0.446,0.   ],
 [0.859,0.457,0.   ],
 [0.861,0.469,0.   ],
 [0.861,0.48 ,0.   ],
 [0.861,0.491,0.   ],
 [0.861,0.502,0.   ],
 [0.861,0.513,0.   ],
 [0.861,0.524,0.   ],
 [0.861,0.535,0.   ],
 [0.861,0.546,0.   ],
 [0.861,0.557,0.   ],
 [0.861,0.569,0.   ],
 [0.861,0.58 ,0.   ],
 [0.861,0.591,0.   ],
 [0.861,0.602,0.   ],
 [0.861,0.613,0.   ],
 [0.861,0.624,0.   ],
 [0.861,0.635,0.   ],
 [0.861,0.646,0.   ],
 [0.863,0.657,0.   ],
 [0.863,0.669,0.   ],
 [0.863,0.68 ,0.   ],
 [0.863,0.691,0.   ],
 [0.863,0.702,0.   ],
 [0.863,0.713,0.   ],
 [0.863,0.724,0.   ],
 [0.863,0.735,0.   ],
 [0.863,0.746,0.   ],
 [0.863,0.757,0.   ],
 [0.863,0.769,0.   ],
 [0.863,0.78 ,0.   ],
 [0.863,0.791,0.   ],
 [0.863,0.802,0.   ],
 [0.863,0.813,0.   ],
 [0.863,0.824,0.   ],
 [0.863,0.835,0.   ],
 [0.863,0.846,0.   ],
 [0.863,0.857,0.   ],
 [0.865,0.869,0.   ],
 [0.865,0.88 ,0.   ],
 [0.865,0.891,0.   ],
 [0.844,0.907,0.   ],
 [0.822,0.907,0.   ],
 [0.8  ,0.907,0.   ],
 [0.778,0.907,0.   ],
 [0.761,0.906,0.   ],
 [0.739,0.906,0.   ],
 [0.717,0.906,0.   ],
 [0.694,0.906,0.   ],
 [0.672,0.906,0.   ],
 [0.652,0.904,0.   ],
 [0.63 ,0.904,0.   ],
 [0.607,0.904,0.   ],
 [0.585,0.904,0.   ],
 [0.563,0.904,0.   ],
 [0.541,0.904,0.   ],
 [0.519,0.904,0.   ],
 [0.498,0.902,0.   ],
 [0.476,0.902,0.   ],
 [0.454,0.902,0.   ],
 [0.431,0.902,0.   ],
 [0.409,0.902,0.   ],
 [0.387,0.902,0.   ],
 [0.365,0.902,0.   ],
 [0.344,0.9  ,0.   ],
 [0.322,0.9  ,0.   ],
 [0.3  ,0.9  ,0.   ],
 [0.278,0.9  ,0.   ],
 [0.256,0.9  ,0.   ],
 [0.235,0.898,0.   ],
 [0.213,0.898,0.   ],
 [0.191,0.898,0.   ],
 [0.169,0.898,0.   ],
 [0.146,0.898,0.   ],
 [0.124,0.898,0.   ],
 [0.102,0.898,0.   ],
 [0.08 ,0.898,0.   ],
 [0.059,0.896,0.   ],
 [0.069,0.057,0.   ],
 [0.089,0.059,0.   ],
 [0.111,0.059,0.   ],
 [0.133,0.059,0.   ],
 [0.156,0.059,0.   ],
 [0.178,0.059,0.   ],
 [0.2  ,0.059,0.   ],
 [0.222,0.061,0.   ],
 [0.244,0.061,0.   ],
 [0.267,0.061,0.   ],
 [0.289,0.061,0.   ],
 [0.311,0.061,0.   ],
 [0.333,0.061,0.   ],
 [0.356,0.061,0.   ],
 [0.378,0.061,0.   ],
 [0.4  ,0.061,0.   ]
]

path =[]
#ox=0.509
#oy=0.063
#for p in opath:
#    path.append([(p[0]-ox)*2,(p[1]-oy)*2,p[2]])
#path =[]
###sin
###for i in range(50):
###    path.append([i*0.05,0.5*sin(i*0.3),0.0])
center=[0.0,-0.5]
r=0.5
for i in np.linspace(0, (2*np.pi)*11/12, 100):
    path.append([center[0]+r*math.sin(i),center[1]+r*math.cos(i),0.0])
path=np.array(path)

service_flag=False
#service_flag=True

class pathfolloing_node(Node):
    """
    送信側
    """

    # ノード名
    SELFNODE = "mpc_pathfolloing"

    def __init__(self):
        """
        コンストラクタ
        Parameters
        ----------
        """
        # ノードの初期化
        super().__init__(self.SELFNODE)
        self.mpc = MPC(5.0,0.1,0.3,1.5)#予測ホライズンの長さ[s],dt[s],並進最大速度,最大角速度
        #トピック設定
        self.odomsub = self.create_subscription(Odometry,'odom', self.odomsub_callback, 10)
        self.pathsub = self.create_subscription(Path,'path', self.pathsub_callback, 10)
        self.twistpub = self.create_publisher(Twist,'cmd_vel', 10)
        self.optipathpub = self.create_publisher(Path,'opti_path', 10)
        self.movepathpub = self.create_publisher(Path,'move_path', 10)
        #サービス設定
        self.cli = self.create_client(SetBool, '/motor_power')
        if service_flag:
            while not self.cli.wait_for_service(timeout_sec=1.0):
                self.get_logger().info('service not acailable waiting again...')

        #各種変数初期化
        self.nowpos=[0,0,0]
        self.nowvel=[0,0,0]
        self.odomflag=False
        self.pathflag=False
        self.path=[]

        self.movepath=Path()
        self.movepath.header.stamp = self.get_clock().now().to_msg()
        self.movepath.header.frame_id = "map"
        self.movepath.poses=[]

        self.path=self.mpc.conversion_path(path)#テスト経路代入
        #t=np.arange(len(self.path))
        #plt.plot(t,self.path[:,0],color = "red")
        #plt.plot(t,self.path[:,1],color = "blue")
        #plt.plot(t,self.path[:,2],color = "green")
        #plt.plot(t,self.path[:,3],color = "m")
        ##plt.plot(t,self.path[:,4],color = "")
        #plt.plot(t,self.path[:,5],color = "y")
        #plt.grid()
        #plt.show()

        self.pathflag=True
        self.pathpub = self.create_publisher(Path,'path_', 10)
        self.create_timer(0.001, self.callback)

    def __del__(self):
        """
        デストラクタ
        """
        self.get_logger().info("%s done." % self.SELFNODE)

    def odomsub_callback(self, val):#自己位置取得
        self.nowpos=[val.pose.pose.position.x,val.pose.pose.position.y,euler_from_quaternion(val.pose.pose.orientation.x,val.pose.pose.orientation.y,val.pose.pose.orientation.z,val.pose.pose.orientation.w)[2]]
        self.nowvel=[val.twist.twist.linear.x,val.twist.twist.linear.y,val.twist.twist.angular.z]
        #self.nowvel=[val.twist.twist.linear.x,val.twist.twist.angular.z]
        print(self.nowpos,val.twist.twist)
        self.odomflag=True

    def pathsub_callback(self, msg):#目標経路取得
        size=len(msg.poses)
        path=[]
        for i in range(size):
            x = msg.poses[i].pose.position.x
            y = msg.poses[i].pose.position.y
            theta = euler_from_quaternion(msg.pose.orientation.x,msg.pose.pose.orientation.y,msg.pose.pose.orientation.z,msg.pose.pose.orientation.w)[2]
            path.append([x,y,theta])
        self.path=self.mpc.conversion_path(path)#経路補完と目標速度計算
        #移動経路の記録リセット
        self.movepath.poses=[]
        #self.movepath.poses.append(movepose)
        self.movepathpub.publish(self.movepath)#通った経路出力
        self.pathflag=True

    def callback(self):
        #追従経路出力 デバック用
        path_=Path()
        path_.header.stamp = self.get_clock().now().to_msg()
        path_.header.frame_id = "map"
        for i in range(len(self.path)):
            pose = PoseStamped()
            pose.pose.position.x = self.path[i][0]
            pose.pose.position.y = self.path[i][1]
            path_.poses.append(pose)
        self.pathpub.publish(path_)
        if self.pathflag and self.odomflag:
            #最適化計算
            w_opt, tgrid = self.mpc.solve(self.nowpos,self.nowvel,self.path)
            x1_opt = np.array(w_opt[0::6])
            x2_opt = np.array(w_opt[1::6])
            #x3_opt = np.array(w_opt[2::6])
            u1_opt = np.array(w_opt[3::6])
            #u2_opt = np.array(w_opt[4::6])
            u3_opt = np.array(w_opt[5::6])
            #最適化結果出力　デバック用
            optipath=Path()
            optipath.header.stamp = self.get_clock().now().to_msg()
            optipath.header.frame_id = "map"
            for i in range(len(x1_opt)):
                pose = PoseStamped()
                pose.pose.position.x = x1_opt[i]
                pose.pose.position.y = x2_opt[i]
                optipath.poses.append(pose)
            self.optipathpub.publish(optipath)
            #制御入力設定
            vel = Twist()
            vel.linear.x=u1_opt[0]
            vel.linear.y=0.0
            vel.linear.z=0.0
            vel.angular.x=0.0
            vel.angular.y=0.0
            vel.angular.z=u3_opt[0]
            self.twistpub.publish(vel)
            self.get_logger().info('twist '+str(u1_opt[1])+' '+str(u3_opt[1]))
            #収束判定
            if(self.mpc.goal_check(0.03,0.03)):
                self.pathflag=self.odomflag=False
                self.get_logger().info('goal!')
                vel = Twist()
                self.twistpub.publish(vel)
            #通った経路の削除
            #self.path=self.path[self.mpc.min_index():,:]
            #通った経路出力
            movepose = PoseStamped()
            movepose.pose.position.x = self.nowpos[0]
            movepose.pose.position.y = self.nowpos[1]
            self.movepath.poses.append(movepose)
            self.movepathpub.publish(self.movepath)

    def send_request(self,b):
        if service_flag:
            srv = SetBool.Request()
            srv.data = b
            self.future = self.cli.call_async(srv)
            rclpy.spin_until_future_complete(self,self.future)
            return self.future.result()


def main(args=None):
    try:
        # rclpyの初期化
        rclpy.init(args=args)
        # インスタンスを生成
        node=pathfolloing_node()
        if service_flag:
            node.send_request(True)
        # プロセス終了までアイドリング
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    finally:
        node.send_request(False)
        # 終了処理
        rclpy.shutdown()


if __name__ == '__main__':
    main()