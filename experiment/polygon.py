import numpy as np
from numpy import linalg as LA
import abc
import sys
import cv2

class Polygon(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def judge(self, cnt):
        """
        Parameters
        ----------
        `cnt` : np.ndarray
            n角形の輪郭．点の数はn
        """
        raise NotImplementedError
    
    @abc.abstractmethod
    def update(self, cnt):
        """
        Parameters
        ----------
        `cnt` : np.ndarray
            n角形の輪郭．点数はn
        """
        raise NotImplementedError




class Triangle(Polygon):
    # area threshold
    area_lower_th = 0.8
    area_upper_th = 1.2
    edge_num = 3

    def __init__(self, init_area=100, init_center=(0,0), init_angle=0):
        """
        Parameters
        ----------
        `init_center` : tuple or list etc..
            重心座標の初期値（使ってないけど一応）
        `init_angle` : float
            向きの初期値（使ってないけど一応）
        """
        self.center = np.array(init_center, np.float64)
        self.angle = np.float64(init_angle)
        
        self.cnt = None
        self.area = None
    
    # override
    def judge(self, cnt):
        area = cv2.contourArea(cnt)
        if Triangle.area_lower_th*self.area < area and area < Triangle.area_upper_th*self.area:
            return True
        return False

    # override
    def update(self, cnt):
        en = Triangle.edge_num
        cnt = np.reshape(cnt, (-1,2))
        # radian
        angle_min = 3.14
        # 多角形の向き．鋭い角の二等分線
        angle_mean = 0 
        # 最も鋭い角を求める
        for i in range(cnt.shape[0]):
            v1:np.ndarray = cnt[(i+1)%en] - cnt[i]
            v2:np.ndarray = cnt[(i+1)%en] - cnt[(i+2)%en]
            angle = self.culc_angle_2vec(v1, v2)
            if angle < angle_min:
                angle_min = angle
                angle_mean = self.mean_angle_2vec(v1, v2)
        self.center = cnt.mean(axis=0)
        self.angle = angle_mean
        return self.center, self.angle
        
    def culc_angle_2vec(self, v1, v2):
        """
        2ベクトルの角度を返す
        """
        i = np.inner(v1, v2)
        n = LA.norm(v1) * LA.norm(v2)
        # radian
        return np.arccos(np.clip(i/n, -1, 1))
        # degree
        # return np.rad2deg(np.arccos(np.clip(i/n, -1, 1)))

    def mean_angle_2vec(self, v1, v2):
        """
        2ベクトルを正規化し，その合成ベクトルの角度を返す
        """
        # 正規化
        v1_ = v1 / LA.norm(v1)
        v2_ = v2 / LA.norm(v2)
        v = v1_ + v2_
        # radian
        return np.arctan2(v[1], v[0])
        # degree
        # return np.rad2deg(np.arctan2(v[0], v[1]))

    def calc_dist(self, cnt):
        center = np.reshape(cnt, [-1,2]).mean(axis=0)
        dist = np.sqrt(((self.center - center)**2).sum())
        return dist
        


class Rectangle():
    edge_num = 4
    # area_lower_th = 0.8
    # area_upper_th = 1.2
    def __init__(self, init_center, src_pt):
        """
        Parameters
        ----------
        `init_center` : tuple or list etc...
            重心座標の初期値
        """
        self.center = np.array(init_center, dtype=np.float64)
        self.src_pt = np.array(src_pt, dtype=np.float64)
        self.dist_th = 10000 # 適当

    # override
    def judge(self, cnt): 
        dist = self.calc_dist(cnt)
        if dist < self.dist_th:
            return True
        return False

    # override 
    def update(self, cnt):
        self.center = np.reshape(cnt, [-1,2]).mean(axis=0)
        self.cnt = cnt
        pass

    def set_dist_th(self, dist_th):
        self.dist_th = dist_th
    
    def calc_dist(self, cnt):
        center = np.reshape(cnt, [-1,2]).mean(axis=0)
        dist = np.sqrt(((self.center - center)**2).sum())
        return dist

    def calc_dist_rect(self, rect):
        dist = np.sqrt(((self.center - rect.center)**2).sum())
        return dist

    def get_src_dst(self):
        """
        Returns:
        `self.src_pt` : np.ndarray dtype=float64
            元の座標
        `self.center` : np.ndarray dtyp=float64
            現在の座標
        """
        return self.src_pt, self.center




class ManageRectangles:
    def __init__(self, rectangle_list:list):
        # Rectangleのリスト
        self.rl = rectangle_list.copy()
        pass

    def update(self, found_rectangle:list):
        """
        Parameters
        ----------
        `found_rectangles` : list
            輪郭データのリスト

        Returns
        -------
        `updated_markers` : list
            update された，すなわち今回の輪郭の中に同じものがあった marker が返される
        """
        updated_rectangle = []
        _rl = self.rl.copy()
        found_rectangle = sorted(found_rectangle, key=lambda x:cv2.contourArea(x), reverse=True)
        # print(f"{found_rectangle=}")
        for fp in found_rectangle:
            # dubug
            # print(f"{fp=}")
            # print(f"{found_rectangle=}")
            # print(f"{_rl[0].calc_dist(fp)=}")
            rl = sorted(_rl, key=lambda rect : rect.calc_dist(fp))
            for rect in rl:
                if rect.judge(fp):
                    rect.update(fp)
                    _rl.remove(rect)
                    updated_rectangle.append(rect)
                    # print("update!!!!!")
                    break
        for rect1 in self.rl:
            _rl = self.rl.copy()
            _rl.remove(rect1)
            rect2 = min(_rl, key=lambda rect2:rect1.calc_dist_rect(rect2))
            dist_min = rect1.calc_dist_rect(rect2)/2
            # print(f"{dist_min=}")
            rect1.set_dist_th(dist_min)
            
        # print("updated_rectangle")
        # print(updated_rectangle)
        return updated_rectangle
            
