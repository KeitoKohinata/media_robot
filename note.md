# Note

<!--  -->
## 構成

- GeometryFromCam
  - ノード．画像を輪郭に変換する．輪郭を多角形に変換する．それをManagementPolygonに投げる．
  - init
  - cam_setting
  - window_setting
  - marker_setting
  - calibration
  - timer_callback
  - close
  - preprocess_img
  - find_polygon
  - scaling(src_pts) : 主に三角形の中心座標を適切な位置に変換するのに使う．homographyだと，どうしても左上のところが中心になってしまう似で，左下を中心となるようにする．また，角度の正負を反転させる．

- ManagementRectangles
  - Polygonのリストを管理する．Polygonのリストを受け取る．
  - update(now_polygon_data) : 輪郭データを多角形近似したデータを受け取り，Polygon のリストに含まれる各ポリゴンを更新する．更新方法
    - contoursをcontourAreaでソート
    - for poly in polys:
      - cntと，各Polygonの距離でソート
      - for 最も近いPolygonに対して， polygon.judge(poly)を呼び出し，Trueならpolygon.update(poly)してcontinue．Falseなら次に進む．
      - return : updateされたpolygonのindex

- Homography
  - 各Polygonからsrc_ptを受け取り，それを使ってHomography行列を計算する．最小二乗法を使う．点数に関係なくHomographyを計算する．点数が0の場合は，計算しない．Noneでも返す．
  - calc_homography(src_pts, dst_pts) : homography行列を返す．何もない場合はNoneを返す．self.homographyとして保存しておく
  - homography_trans(str_pts) -> dst_pts : 入力点を，homography行列で座標変換し，その結果を返す．



- Polygon 
  - 抽象クラス．TriangleとRectangleに継承される．
  - judge : それが前に受け取った多角形と等しいかどうか
  - update : judeg の結果あるいは何らかの基準により，多角形に関するデータを保存したい場合に呼び出す
  - 

- Triangle
  - Polygonを継承する
  - 

- Rectangle
  - Polygonを継承する
  - judge : 
    - 見つかったマーカーの面積が前のやつの半分以下とかある値以下なら異なるマーカーとみなす．
    - 隣合うマーカーで，一番近いマーカーとの距離の半分を前の状態で保持しておく．それよりも距離が大きいなら異なるマーカーとみなす．


    

## 思ったこと．疑問など


- contourArea は負の面積をもとめられないか？
- 凸性の判定は全てのとおりに対して1意に定まるか？実際に検証したい．



## 後で考えること

- マーカーは4角形ではなくて十字とかのほうが簡単で精度が高くていいかもしれない
- マーカーの内側に，エッジに対して3/4 (4/5)の大きさの正方形を考える．


# 敗因

反射しやすいテープで自己位置を推定しようとしてしまった