# v4l2-ctl  --list-devices
# v4l2-ctl -d /dev/video0 --list-formats-ext

import pathlib 
import sys

PARENT = pathlib.Path(__file__).resolve().parent
sys.path.append(str(PARENT))

import cv2
import numpy as np
from numpy import linalg as LA
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
import argparse
from tf_lib import quaternion_from_euler
from poly import Triangle, Rectangle

# オレンジ色抽出用
# LOWER_BOUND = np.array([0, 75, 75])
# UPPER_BOUND = np.array([20, 255, 255])

# green hsv 養生テープ in my room 抽出用
LOWER_BOUND = np.array([30, 30, 40])
UPPER_BOUND = np.array([80, 255, 255])



class GeometryFromCam(Node):
    def __init__(self, mode="ros2"):
        """
        mode : ros2 or other
        """
        super().__init__("geometry_from_cam")
        self.mode = mode
        # 場合によって変更する必要があるが，pcに繋いでいる以上変更する必要はない
        self.cam_number = 2
        self.cam_setting()
        self.window_setting()
        # ロボットのマーカーは中心にあると仮定
        self.pos = Triangle(pc = (self.cam_width/2, self.cam_height/2))
        self.marker_setting()
        # ros2 init
        self.publisher = self.create_publisher(Odometry, "/odom_from_cam", 10)
        ## なるべく速く実行するため
        timer_period = 0.001 
        self.timer = self.create_timer(timer_period, self.timer_callback)
        # キャリブレーション
        self.calibration()

    def cam_setting(self):
        # カメラの設定
        self.cam_width = 640
        self.cam_height = 480
        self.cam = cv2.VideoCapture(self.cam_number)
        self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, self.cam_width)
        self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, self.cam_height)
        self.cam.set(cv2.CAP_PROP_FPS, 30)
        print(f"{self.cam.get(cv2.CAP_PROP_FRAME_WIDTH)=}")
        print(f"{self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT)=}")
        print(f"{self.cam.get(cv2.CAP_PROP_FPS)=}")
        
    def window_setting(self):
        # window
        cv2.namedWindow("img")
        cv2.namedWindow("img_bin")
        cv2.moveWindow("img", 50, 13)
        cv2.moveWindow("img_bin", 700, 50)
        # cv2.setWindowProperty("img", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        # cv2.setWindowProperty("img_bin", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

    def marker_setting(self):
        aspect = [self.cam_width, self.cam_height]
        bi = aspect.index(max(aspect))
        si = aspect.index(min(aspect))
        # 正方形の1辺の長さ
        edge = aspect[si]
        self.edge = edge
        # marker の margin
        self.margin_marker = 0.1
        # marker の source． 変えない．
        self.src_pts_markers = np.array([
            [0,0],
            [1, 0],
            [1, 1],
            [0,1],
        ], np.float64)
        # 1辺がedgeの正方形
        init_coor = self.src_pts_markers*edge
        # 中央に寄せる
        init_coor[:, bi] += int((aspect[bi]-edge)/2)
        # 更に中央に寄せる
        init_coor -= (self.src_pts_markers-0.5)*2*self.margin_marker*edge
        print(f"{init_coor=}")

        self.markers = [Rectangle(coor) for coor in init_coor]

    def calibration(self):
        # img, img_binを表示する．imgには，合わせるべきマーカーの位置を表示する．
        # 画像を回しながらロボットのいるべき座標と，マーカーの番号を画面に表示する．(calib状態)
        # 人間が，「それらが合わさった」と判断したなら，Enterを押す．（check状態への以降）
        # 画像前処理と多角形取得をして，ロボットの位置，マーカーの番号を表示する．（check状態)
            # それでよかったら，処理を終了する
            # よくなかったら，R を押してcalib状態に戻る．
        ret, img = self.cam.read()
        while ret:
            check_flag = False
            # calib
            while ret:
                self.preprocess_img(img)
                for marker in self.markers:
                    lt = (marker.pc - self.edge*self.margin_marker/2).astype(int)
                    rb = (marker.pc + self.edge*self.margin_marker/2).astype(int)
                    cv2.rectangle(img, lt, rb, (255,255,255), 3, cv2.LINE_AA)
                lt = (self.pos.pc - self.edge*self.margin_marker/2).astype(int)
                rb = (self.pos.pc + self.edge*self.margin_marker/2).astype(int)
                cv2.rectangle(img, lt, rb, (255,255,255), 3)
                cv2.imshow("img", img)
                key = cv2.waitKey(1) 
                ret, img = self.cam.read()
                if key == ord("q"):
                    quit()
                elif key == ord("n"):
                    break
            # check
            while True:
                key = self.timer_callback()
                if key == ord("q"):
                    quit()
                elif key == ord("n"):
                    check_flag = True
                    break
                elif key == ord("r"):
                    break
            if check_flag:
                break
        pass

    def timer_callback(self):
        # 画像読み込み
        ret, img = self.cam.read()
        # 読み込み不能の場合
        if not ret: 
            return 
        # 画像前処理
        img_bin = self.preprocess_img(img)
        # 多角形の取得
        polygon:dict = self.find_polygon(img_bin)
        # 多角形の内容を描画
        _draw = polygon.get(3,[])+polygon.get(4,[])
        cv2.drawContours(img, _draw, -1, (255,255,255), 2)
        # 現在位置と角度を取得．更新
        rects = sorted(polygon.get(3, []), key=lambda x:cv2.contourArea(x), reverse=True)
        pos = self.pos.save(rects[:4])
        # マーカーの位置を更新
        # TODO : マーカーに位置を更新させない場合を用意する．
        for i,m in enumerate(self.markers):
            c = m.save(polygon.get(4,[])).astype(int)
            cv2.putText(img, str(i), c, cv2.FONT_HERSHEY_SIMPLEX, 2, (255,255,255), 2, cv2.LINE_AA)
        # publish
        if pos != None and self.mode == "ros2": 
            odom = Odometry()
            pose = Pose()
            odom.header.stamp = self.get_clock().now().to_msg()
            pose.position.x = pos[0][0]
            pose.position.y = pos[0][1]
            q = quaternion_from_euler(0,0,pos[1])
            pose.orientation.x = q[0]
            pose.orientation.y = q[1]
            pose.orientation.z = q[2]
            pose.orientation.w = q[3]
            odom.pose.pose = pose
            self.publisher.publish(odom)
            pass
        # poseの位置と角度を意味する楕円をimgに描画
        if pos != None:
            cv2.ellipse(img, (int(pos[0][0]), int(pos[0][1])) , (100,100), 0, 0, np.rad2deg(pos[1]), (0,255,0), -1)
        cv2.imshow("img", img)
        key = cv2.waitKey(1)
        return key

    def close(self):
        #TODO : save と 最終的な軌跡の表示？それは
        cv2.destroyAllWindows()
        self.cam.release()
        pass

        

    def preprocess_img(self, img):
        """
        ぼかし，2値化等
        """
        ks = 5
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (ks,ks))
        # img = cv2.GaussianBlur(img, (ks,ks), 0)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        img = cv2.inRange(img, LOWER_BOUND, UPPER_BOUND)
        img = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
        img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
        # debug
        cv2.imshow("img_bin", img)
        return img
    
    # 多角形を見つける
    def find_polygon(self, img_bin):
        """
        Returns
        -------
        `result` : dict
            多角形のdict．{3:輪郭のリスト, 4:輪郭のリスト, ...} みたいな感じ．
        """
        _find_polygon_arclength_ratio = 0.1
        contours, _ = cv2.findContours(img_bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        result = dict()
        for cnt in contours:
            polygon:np.ndarray = cv2.approxPolyDP(cnt, _find_polygon_arclength_ratio*cv2.arcLength(cnt, closed=True), closed=True)
            result.setdefault(polygon.shape[0], [])
            result[polygon.shape[0]].append(polygon)
        return result

    # debug
    # def draw_polygon(self, img, polygon):
        # cv2.drawContours(polygon)




def parseOpt():
    """
    オプション処理
    """
    parser = argparse.ArgumentParser()

    # よく使うもの
    ## 確実に必要
    parser.add_argument("--mode", "-m", type=str, default="ros2")
    # 辞書化
    opt: dict = vars(parser.parse_args())
    return opt


def main(args=None):
    rclpy.init(args=args)
    opt = parseOpt()
    # ros2を使わないデバッグ時は --mode hoge とすると良い．
    gfc = GeometryFromCam(opt["mode"])
    rclpy.spin(gfc)

    # after ctrl+c 
    gfc.close()
    gfc.destroy_node()
    rclpy.shutdown()




if __name__=="__main__":
    main()
# %%
