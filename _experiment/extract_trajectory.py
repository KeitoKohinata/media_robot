# %%
# v4l2-ctl --list-devices
import cv2
import numpy as np

# orange hsv
lower_bound = np.array([150, 20, 20])
upper_bound = np.array([180, 180, 180])



class ExtractTrajectory:
    def __init__(self):
        # カメラの設定
        cap = cv2.VideoCapture(2)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        cap.set(cv2.CAP_PROP_FPS, 30)
        print(f"{cap.get(cv2.CAP_PROP_FRAME_WIDTH)=}")
        print(f"{cap.get(cv2.CAP_PROP_FRAME_HEIGHT)=}")
        print(f"{cap.get(cv2.CAP_PROP_FPS)=}")
        # ウィンドウの設定
        cv2.namedWindow("img")
        cv2.namedWindow("img_bin")
        cv2.moveWindow("img", 50,50)
        cv2.moveWindow("img_bin", 750,50)
        ret, img = cap.read()
        key = -1
        while ret and key != ord("q"):
            img = cv2.GaussianBlur(img, (7,7), sigmaX=0)
            img_bin = cv2.inRange(img, lower_bound, upper_bound)
            cv2.imshow("img", img)
            cv2.imshow("img_bin", img_bin)
            key = cv2.waitKey(1)
            ret, img = cap.read()
        cv2.destroyAllWindows()
        cap.release()

def main():
    ExtractTrajectory()

if __name__=="__main__":
    main()