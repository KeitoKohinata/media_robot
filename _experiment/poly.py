import numpy as np
from numpy import linalg as LA
import sys

class Triangle ():
    def __init__(self, pc=(0,0), pa=0):
        # 三角形を表す
        self.poly = 3
        # １フレーム前の座標
        # 三角形の初期座標を設定
        self.pc = np.array(pc, np.float64)
        self.pa = pa
        # 三角形が複数存在する場合に選択する方法
        # self.select_nearest = self.select_nearest1
        pass

    def save(self, cnts: dict):
        """
        Returns
        -------
        `res` : list
            [center, angle]
            `center` : np.ndarray
                画像上の座標 [x,y]
            `angle` : float
                多角形の向き
        """
        poly = self.poly
        if len(cnts)==0:
            return self.pc, self.pa
        triangles = []
        for _cnt in cnts:
            cnt = np.reshape(_cnt, (-1,2))
            # radian
            angle_min = 3.14
            # 多角形の向き
            angle_mean = 0 # 適当な初期値
            # 最も鋭い角を求める
            for i in range(cnt.shape[0]):
                v1:np.ndarray = cnt[(i+1)%poly] - cnt[i]
                v2:np.ndarray = cnt[(i+1)%poly] - cnt[(i+2)%poly]
                angle = self.culc_angle_2vec(v1, v2)
                if angle < angle_min:
                    angle_min = angle
                    angle_mean = self.mean_angle_2vec(v1, v2)
            # 重心（中心）
            center = cnt.mean(0)
            triangles.append([center, angle_mean])
        # 三角形が複数存在する場合に選択する方法
        res: list = self.select_nearest1(triangles)
        # selfの更新
        self.pc = res[0]
        self.pa = res[1]
        return res
    
    def culc_angle_2vec(self, v1, v2):
        """
        2ベクトルの角度を返す
        """
        i = np.inner(v1, v2)
        n = LA.norm(v1) * LA.norm(v2)
        # radian
        return np.arccos(np.clip(i/n, -1, 1))
        # degree
        # return np.rad2deg(np.arccos(np.clip(i/n, -1, 1)))

    def mean_angle_2vec(self, v1, v2):
        """
        2ベクトルを正規化し，その合成ベクトルの角度を返す
        """
        # 正規化
        v1_ = v1 / LA.norm(v1)
        v2_ = v2 / LA.norm(v2)
        v = v1_ + v2_
        # radian
        return np.arctan2(v[1], v[0])
        # degree
        # return np.rad2deg(np.arctan2(v[0], v[1]))
        
    def select_nearest1(self, l: list) -> list:
        # 前の中心座標と最も近いものを選ぶ
        return min(l, key=lambda x : np.sum((x[0]-self.pc)**2))




#  マーカーとなる四角形を取得するクラス
# TODO : マーカーとなる多角形を取得するクラスでアブストラクトクラスを作る
class Rectangle():
    def __init__(self, pc=(0,0)):
        self.poly = 4
        # １フレーム前の座標
        if len(pc) != 2:
            raise ValueError("2次元の必要がある．")
        self.pc = np.array(pc, np.float64)
        pass

    def save(self, cnts):
        """
        Parameters
        ----------
        `cnts` : iterator
            4角形の輪郭

        Returns
        -------
        `center` : np.ndarray
            画像上の座標 [x,y]
        """
        poly = self.poly
        if len(cnts)==0:
            return self.pc
        # 以前の座標と最も近いものを選ぶ
        min_dist = sys.float_info.max
        min_center = self.pc
        for _cnt in cnts:
            cnt = np.reshape(_cnt, (-1,2))
            center = cnt.mean(axis=0)
            dist = ((self.pc-center)**2).sum()
            if min_dist > dist:
                min_dist = dist
                min_center = center
        self.pc = min_center
        return self.pc
            
            